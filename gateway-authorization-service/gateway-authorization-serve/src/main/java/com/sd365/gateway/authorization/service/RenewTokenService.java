/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: RenewTokenService
 * Author: Administrator
 * Date: 2023-04-26 15:20:46
 * Description:
 * 将token续约功能从鉴权服务分离出来优化代码结构提高代码复用度
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-26 15:20:46
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.authorization.service;

/**
 * @InterfaceName: RenewTokenService
 * @Description: 这里描述接口
 * @Author: Administrator
 * @Date: 2023-04-26 15:20
 **/
public interface RenewTokenService {

    /**
     * 如果过期则请求该接口续约
     * @param userId  用户id 前端续约发起
     * @return 签名后的token
     */
   String renewToken(String userId);

    /**
     *  判断token是否过期
     * @param token 请求头中的token
     * @return 1-TOKEN_EXPIRE_TURE 过期  0 即将过期 -TOKEN_EXPIRE_NEARLY -1 没有过期-TOKEN_EXPIRE_FALSE
     */
   int expire(Long userId,String token);

}
