/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: UserService
 * Author: Administrator
 * Date: 2023-05-10 14:45:46
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-05-10 14:45:46
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.authorization.service.remote;

import com.sd365.permission.centre.pojo.vo.ResourceVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @ClassName: UserService
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-05-10 14:45
 **/
@FeignClient("sd365-permission-center")
public interface UserService {
    /**
     *  根据用户id请求所拥有的资源列表
     * @param roleIdArray 用户id
     * @return 权限列表
     */
    @GetMapping(value = "/getRoleResource")
    List<ResourceVO> getRoleResourceVO(Long[] roleIdArray);
}
