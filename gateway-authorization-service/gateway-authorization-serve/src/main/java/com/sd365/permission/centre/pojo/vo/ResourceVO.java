package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * ResourceVO类，相对于Resource类，多了个ResourceVOs列表属性，用来表示子资源
 * @author No_one
 * @createTime 2020/12/12
 */
@ApiModel(value="com.sd365.permission.centre.entity.Resource")
@Table(name = "basic_resource")
@Data
public class ResourceVO extends TenantBaseEntity {
    private ResourceVO resourceVO;

    public void setResourceVO(ResourceVO resourceVO) {
        this.resourceVO = resourceVO;
    }

    /**
     * 资源的子资源（子结点）
     */
    @Transient
    @ApiModelProperty(value = "resourceVOs子资源")
    private List<ResourceVO> resourceVOs;

    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    @ApiModelProperty(value="name资源名称  注意这个也可以是界面上的按钮")
    private String name;

    /**
     * 子系统ID，预留
     */
    @ApiModelProperty(value="subSystemId子系统ID，预留")
    private Long subSystemId;

    /**
     * 编号
     */
    @ApiModelProperty(value="code编号 ")
    private String code;

    /**
     * 顺序号，控制菜单显示顺序
     */
    @ApiModelProperty(value="orderIndex顺序号，控制菜单显示顺序")
    private Integer orderIndex;

    /**
     * 父亲节点
     */
    @ApiModelProperty(value="parentId父亲节点")
    private Long parentId;

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    @ApiModelProperty(value="urlURL 这个决定加载什么组件 对应import组件")
    private String url;

    /**
     * 后端接口地址读音组件的 path属性
     */
    @ApiModelProperty(value="api后端接口地址对应后端的requestmaping")
    private String api;

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    @ApiModelProperty(value="method请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE")
    private Byte method;

    /**
     * 打开图标 这个只能取系统预定义的
     */
    @ApiModelProperty(value="openImg打开图标 这个只能取系统预定义的")
    private String openImg;

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    @ApiModelProperty(value="closeImg关闭图标  这个只能取系统预定义的")
    private String closeImg;

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    @ApiModelProperty(value="resourceType资源类型  这个是菜单还是按钮 0 菜单 1 按钮  ")
    private Byte resourceType;

    /**
     * 叶子节点
     */
    @ApiModelProperty(value="leaf叶子节点")
    private Byte leaf;

    /**
     * 动作 这个暂时保留
     */
    @ApiModelProperty(value="action动作 这个暂时保留")
    private Byte action;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    public String getName() {
        return name;
    }

    /**
     * 资源名称  注意这个也可以是界面上的按钮
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 子系统ID，预留
     */
    public Long getSubSystemId() {
        return subSystemId;
    }

    /**
     * 子系统ID，预留
     */
    public void setSubSystemId(Long subSystemId) {
        this.subSystemId = subSystemId;
    }

    /**
     * 编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 顺序号，控制菜单显示顺序
     */
    public Integer getOrderIndex() {
        return orderIndex;
    }

    /**
     * 顺序号，控制菜单显示顺序
     */
    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    /**
     * 父亲节点
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 父亲节点
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    public String getUrl() {
        return url;
    }

    /**
     * URL 这个决定加载什么组件 对应import组件
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 后端接口地址读音组件的 path属性
     */
    public String getApi() {
        return api;
    }

    /**
     * 后端接口地址读音组件的 path属性
     */
    public void setApi(String api) {
        this.api = api;
    }

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    public Byte getMethod() {
        return method;
    }

    /**
     * 请求方法，和 api 搭配使用, 1 GET, 2 POST, 3 PUT,4  DELETE
     */
    public void setMethod(Byte method) {
        this.method = method;
    }

    /**
     * 打开图标 这个只能取系统预定义的
     */
    public String getOpenImg() {
        return openImg;
    }

    /**
     * 打开图标 这个只能取系统预定义的
     */
    public void setOpenImg(String openImg) {
        this.openImg = openImg;
    }

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    public String getCloseImg() {
        return closeImg;
    }

    /**
     * 关闭图标  这个只能取系统预定义的
     */
    public void setCloseImg(String closeImg) {
        this.closeImg = closeImg;
    }

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    public Byte getResourceType() {
        return resourceType;
    }

    /**
     * 资源类型  这个是菜单还是按钮 0 菜单 1 按钮
     */
    public void setResourceType(Byte resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * 叶子节点
     */
    public Byte getLeaf() {
        return leaf;
    }

    /**
     * 叶子节点
     */
    public void setLeaf(Byte leaf) {
        this.leaf = leaf;
    }

    /**
     * 动作 这个暂时保留
     */
    public Byte getAction() {
        return action;
    }

    /**
     * 动作 这个暂时保留
     */
    public void setAction(Byte action) {
        this.action = action;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
