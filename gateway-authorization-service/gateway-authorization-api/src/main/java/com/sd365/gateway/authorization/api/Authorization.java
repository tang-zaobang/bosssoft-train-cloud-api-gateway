/**
 * FileName: AuthorizationController
 * Author: TangZaoBang
 * Date: 2023/06/05 13:09
 * Description: 当前版本主要是实现鉴权方法以及不需要鉴权的资源查询
 * <p>
 * History:
 * <author> TangZaoBang
 * <time> 2023/06/05 13:09
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.authorization.api;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Class AuthorizationApi
 * @Description 当前版本主要是实现鉴权方法以及不需要鉴权的资源查询
 * @author TangZaoBang
 * @date 2023/06/15 12:18
 * @version 1.0.0
 */
@Api(tags = "鉴权管理", value = "/v1")
@RestController
@RequestMapping(value = "/v1")
public interface Authorization {
    /**
     *  每次请求的目标如果在白名单则要求过此方法进行鉴权，该方法被core服务的全局过滤器调用
     * @param token  由认证服务授权生成的token
     * @param url 请求的目标地址
     * @return true代表通过鉴权 false代表失败
     */
    @ApiOperation(tags = "鉴权判断url",value = "/authorization")
    @GetMapping(value = "/authorization")
    @ResponseBody
    Integer roleAuthorization(String token,String url);

    /**
     *  查询某一些请求是否白名单
     * @param url 请求url
     * @return true 白名单 false黑名单
     */
    @ApiOperation(tags = "开放URL查询",value = "/common/resource")
    @GetMapping(value = "/common/resource")
    @ResponseBody
    Boolean commonResource(String url) ;

    /**
     *  //TODO 前端还未实现续约功能
     *  续约接口，如果认证的结果是token即将过期则提醒前端发起续约请求
     * @param userId 用户id
     * @return 新续约的token
     */
    String renewToken(String userId);
}
