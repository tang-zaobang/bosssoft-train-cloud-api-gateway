package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Table;

@ApiModel(value="RoleResource")
@Table(name = "basic_role_resource")
public class RoleResource extends TenantBaseEntity {
    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 资源ID
     */
    @ApiModelProperty(value="resourceId资源ID")
    private Long resourceId;

    /**
     * 角色ID
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 角色ID
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * 资源ID
     */
    public Long getResourceId() {
        return resourceId;
    }

    /**
     * 资源ID
     */
    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
}