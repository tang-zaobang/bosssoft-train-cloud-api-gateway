package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Table;
import java.util.Date;

@Table(name = "basic_tenant")
public class Tenant extends BaseEntity {
    /**
     * 在租赁平台注册的账号
     */
    @ApiModelProperty(value="account在租赁平台注册的账号")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 名字
     */
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 头像
     */
    @ApiModelProperty(value="profilePicture头像")
    private String profilePicture;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;

    /**
     * 租户属性  0个人用户  1企业用户
     */
    @ApiModelProperty(value="tenantAttr租户属性  0个人用户  1企业用户")
    private Byte tenantAttr;

    /**
     * 租户单位名称  查询条件
     */
    @ApiModelProperty(value="orgName租户单位名称  查询条件")
    private String orgName;

    /**
     * 有效开始时间
     */
    @ApiModelProperty(value="availableTime有效开始时间")
    private Date availableTime;

    /**
     * 过期时间
     */
    @ApiModelProperty(value="expireTime过期时间")
    private Date expireTime;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 
     */
    @ApiModelProperty(value="creator")
    private String creator;

    /**
     * 
     */
    @ApiModelProperty(value="modifier")
    private String modifier;

    /**
     * 在租赁平台注册的账号
     */
    public String getAccount() {
        return account;
    }

    /**
     * 在租赁平台注册的账号
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 名字
     */
    public String getName() {
        return name;
    }

    /**
     * 名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 头像
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     * 头像
     */
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    /**
     * 性别
     */
    public Byte getSex() {
        return sex;
    }

    /**
     * 性别
     */
    public void setSex(Byte sex) {
        this.sex = sex;
    }

    /**
     * 生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 其他联系
     */
    public String getOther() {
        return other;
    }

    /**
     * 其他联系
     */
    public void setOther(String other) {
        this.other = other;
    }

    /**
     * 租户属性  0个人用户  1企业用户
     */
    public Byte getTenantAttr() {
        return tenantAttr;
    }

    /**
     * 租户属性  0个人用户  1企业用户
     */
    public void setTenantAttr(Byte tenantAttr) {
        this.tenantAttr = tenantAttr;
    }

    /**
     * 租户单位名称  查询条件
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * 租户单位名称  查询条件
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 有效开始时间
     */
    public Date getAvailableTime() {
        return availableTime;
    }

    /**
     * 有效开始时间
     */
    public void setAvailableTime(Date availableTime) {
        this.availableTime = availableTime;
    }

    /**
     * 过期时间
     */
    public Date getExpireTime() {
        return expireTime;
    }

    /**
     * 过期时间
     */
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}