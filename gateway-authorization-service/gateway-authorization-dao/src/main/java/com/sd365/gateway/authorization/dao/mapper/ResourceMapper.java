package com.sd365.gateway.authorization.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Resource;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
/**
 * @Interface ResourceMapper
 * @Description 通过本服务链接数据库取得 将来版本将剔除这个
 * @Author Administrator
 * @Date 2023-04-28  13:58
 * @version 1.0.0
 */
@Mapper
public interface ResourceMapper extends CommonMapper<Resource> {
    List<Resource> commonResource();
}