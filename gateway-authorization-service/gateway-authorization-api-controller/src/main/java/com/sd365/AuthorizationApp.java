package com.sd365;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@ComponentScans({
        @ComponentScan("com.sd365.common"),
        @ComponentScan("com.sd365.common.api.swagger")
})
@MapperScan(("com.sd365.gateway.authorization.dao.mapper"))
@EnableCaching
@EnableSwagger2
@EnableDiscoveryClient
@RefreshScope
public class AuthorizationApp
{
    public static void main( String[] args )
    {
        // 禁用nacos的logback 否则将出现 logback context重名失败
        System.setProperty("nacos.logging.default.config.enabled","false");
        SpringApplication.run(AuthorizationApp.class, args);
    }
}
