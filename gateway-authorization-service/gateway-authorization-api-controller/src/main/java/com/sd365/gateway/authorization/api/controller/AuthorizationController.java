/**
 * FileName: AuthorizationController
 * Author: TangZaoBang
 * Date: 2023/06/05 13:09
 * Description: 当前版本主要是实现鉴权方法以及不需要鉴权的资源查询
 * <p>
 * History:
 * <author> TangZaoBang
 * <time> 2023/06/05 13:09
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.authorization.api.controller;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.gateway.authorization.api.Authorization;
import com.sd365.gateway.authorization.service.AuthorizationService;
import com.sd365.gateway.authorization.service.RenewTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Class AuthorizationController
 * @Description 当前版本主要是实现鉴权方法以及不需要鉴权的资源查询
 * @author TangZaoBang
 * @date 2023/06/15 12:18
 * @version 1.0.0
 */
@Slf4j
@RestController
public class AuthorizationController  implements Authorization {
    /**
     *  认证业务服务对象包含主要的鉴权逻辑当前版本的功能较为简单
     */
    @Autowired
    private AuthorizationService authorizationService;
    /**
     * 用于认证失败 token续约
     */
    @Autowired
    private RenewTokenService renewTokenService;

    @Override
    public Integer roleAuthorization(String token, String url) {
        log.debug("roleAuthorization url={}, token={},",url,token);
        BaseContextHolder.set("nowrap", new Object());
        return authorizationService.roleAuthorization(token, url);

    }


    @Override
    public Boolean commonResource(String url)  {
        log.info("roleAuthorization url={}",url);
        BaseContextHolder.set("nowrap", new Object());
        return authorizationService.commonResource(url);
    }

    @Override
    public String renewToken(String userId) {
        return renewTokenService.renewToken(userId);
    }
}
