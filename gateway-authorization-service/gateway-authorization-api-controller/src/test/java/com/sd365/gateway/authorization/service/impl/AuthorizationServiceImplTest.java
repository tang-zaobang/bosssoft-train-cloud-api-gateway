package com.sd365.gateway.authorization.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sd365.AuthorizationApp;

import com.sd365.gateway.authorization.constant.BusinessResultConsts;
import com.sd365.gateway.authorization.service.AuthorizationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthorizationApp.class})
public class AuthorizationServiceImplTest {
    @Resource(name = "tokenRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private AuthorizationService authorizationService;
    @Test
    public void commonResource() {
    }

    @Test
    public void roleAuthorization() {
        String token="{\"typ\":\"JWT\",\"alg\":\"sha2\"}.{\"companyId\":1337960193258029056,\"roleIds\":[\"1337645864428109824\"],\"code\":\"100002\",\"tenantId\":1337940702788714496,\"userName\":\"-1\",\"userId\":1337970508645400576,\"account\":\"BOSSSOFT\",\"orgId\":1337955940401545216,\"expiresAt\":1685700736883}.$5$0123$/C8F9PU0.d5L5.UIB9BsXh/r8IKSN27ubNiifvwWNG/";
        String url="/permission/centre/v1/user";
        Assert.assertTrue(
                authorizationService.roleAuthorization(token,url).equals(BusinessResultConsts.AUTHOR_RESULT_TRUE));
    }

    @Test
    public void getResourcesByRoleIds() {
    }

    @Test
    public void matchRequestUrlWithResources() {
    }

    @Test
    public void directGetRoleResource() {
        Map roleResourceMap= redisTemplate.opsForHash().entries("uc:cache:hash:role:id:1337645864428109824");
        log.info("roleResourceMap size="+roleResourceMap.size());
        Collection collection= roleResourceMap.values();
        log.info("collection values="+collection.size());
        log.info(JSONObject.toJSONString(collection));

    }
}