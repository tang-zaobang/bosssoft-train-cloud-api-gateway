package com.sd365;

import static java.util.regex.Pattern.compile;
import static org.junit.Assert.assertTrue;

import com.sd365.permission.centre.entity.Resource;
import org.junit.Test;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple TMS_PE_WaybillApp.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    /**
     * 测试请求路径解析
     */
    @Test
    public void urlMatch(){
      final String AUTHOR_REQUEST_URL_EXPR="^https?:\\/\\/(?:[0-9a-zA-Z:\\.]*)([^\\?]*)";
      //String url=getMatchURL("http://172.20.2.95:8081/permission/centre/v1/role",AUTHOR_REQUEST_URL_EXPR);

        String url=getMatchURL("http://172.20.2.95:8081/permission/centre/v1/department?companyId=&id=&name=&mnemonicCode=&code=&parentId=&level=&pageSize=10&pageNum=1",
                AUTHOR_REQUEST_URL_EXPR);

      String roleApi="/permission/centre/v1/department($|/[0-9]*)";


      Assert.isTrue( matchRequestUrlWithResources(url,roleApi));
    }

    private String getMatchURL(String url,String reg){
        Assert.hasText(url,"url NOT NULL");
        Assert.hasText(reg,"url NOT NULL");
        String newUrl="";
        final Pattern compile = compile(reg);
        Matcher matcher = compile.matcher(url);
        if (matcher.find()) {
            newUrl = matcher.group(1);
        }
        return newUrl;
    }

    public boolean matchRequestUrlWithResources(String url, String api) {

                final Pattern pattern = compile(String.format("%s$", api));
                final Matcher matcherApi = pattern.matcher(url);
                // 匹配成功
                if (matcherApi.find()) {
                    return Boolean.TRUE;
                }
        // 没有匹配到则返回false
        return Boolean.FALSE;
    }
}
