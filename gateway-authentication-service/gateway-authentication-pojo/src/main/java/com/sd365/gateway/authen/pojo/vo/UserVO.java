/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: UserVO
 * Author: Administrator
 * Date: 2023-04-18 18:46:15
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-18 18:46:15
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.authen.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName: UserVO
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-04-18 18:46
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {
    /**
     * 名字
     */
    private String name;
    /**
     * 电话号码
     */
    private String tel;
    /**
     * 主键雪花算法生成
     */
    private Long id;
    /**
     * 消息和错误码 这个设计不号，返回保卫已经被统一应答包装 CommonResponse的Head包含了 code和message 这里不需要了
     */
    private String msg;
    private int code;

    private Long tenantId;
    private String companyAddress;
    private Long orgId;
    private Long companyId;
    public ResponseData data;
    private List<Long> roleIds;

    @Data
    class ResponseData {
        /**
         * 生成的token ，token在认证服务生成比较合理
         */
        private String token;
        /**
         *  一个用户可以拥有多个角色这里是id
         */
        private List<String> roles;
        /**
         *  名字
         */
        private String name;
        /**
         * 头像地址
         */
        private String avatar;
        /**
         *  用户所拥有的资源，如果在 Role对象中包含这个会更好
         */
//        private List<ResourceVO> resourceVO;
    }
}
