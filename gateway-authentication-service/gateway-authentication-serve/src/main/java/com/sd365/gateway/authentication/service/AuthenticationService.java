package com.sd365.gateway.authentication.service;

import com.sd365.common.core.common.api.CommonResponse;
import com.sd365.gateway.authen.pojo.vo.UserVO;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;

/**
 * @Class AuthenticationService
 * @Description 认证服务接口，判断用户存在的逻辑在用户中心做，但是JWT规范生成token在认证服务做
 * @Author Administrator
 * @Date 2022-10-12  20:05
 * @version 1.0.0
 */
@Validated
public interface AuthenticationService {
    int LOGIN_VERIFY_CODE_SUCCESS=1;

    /**
     * 认证服务生成token并存储到redis中
     * @author TangZaoBang
     * @date 2023/06/14 16:42
     * @param code 登录的账号
     * @param account 密码
     * @param password 租户
     * @return java.util.HashMap
     */
    HashMap doAuth(@NotBlank String code, @NotBlank String account, @NotBlank String password);


}
