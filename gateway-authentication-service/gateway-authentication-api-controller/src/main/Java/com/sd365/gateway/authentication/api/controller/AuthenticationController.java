package com.sd365.gateway.authentication.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sd365.gateway.authen.pojo.vo.UserVO;
import com.sd365.gateway.authentication.api.AuthenticationApi;
import com.sd365.gateway.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;


/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/17 20:57
 * @className
 * @description
 */
@Slf4j
@RestController
public class AuthenticationController implements AuthenticationApi {
    /**
     * 认证服务
     */
    @Autowired
    private  AuthenticationService authenticationService;

    @Override
    public UserVO doAuth(String code, String account, String password) {
        // 将body数据转为UserVO,body 本质是HashMap
        HashMap authResult= authenticationService.doAuth(code, account, password);
        // 将 HashMap转化为 JSONObject
       return JSON.parseObject(new JSONObject((HashMap)authResult.get("body")).toJSONString(),UserVO.class);


    }
}
