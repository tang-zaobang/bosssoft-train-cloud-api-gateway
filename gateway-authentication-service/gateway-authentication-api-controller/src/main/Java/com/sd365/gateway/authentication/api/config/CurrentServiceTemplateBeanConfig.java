package com.sd365.gateway.authentication.api.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.web.client.RestTemplate;

/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/18 11:54
 * @className
 * @description
 */
@Configuration
public class CurrentServiceTemplateBeanConfig {
    /**
     * @Description: 用户请求用户中心，认证的时候需调用用户中心auth方法
     * @Author: Administrator
     * @DATE: 2022-10-13  9:39
     * @return: 具备负载均衡能力
     */
    @Bean
    @LoadBalanced
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }


    @Bean(name = "tokenRedisTemplate")
    RedisTemplate<String, Object> getRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new Jackson2JsonRedisSerializer<String>(String.class));
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Object>(Object.class));
        return redisTemplate;
    }
}

