/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName AuthenticationControllerTest.java
 * @Author Administrator
 * @Date 2022-10-13  13:47
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-10-13  13:47
 * <version> 1.0.0
 * <desc> 该文件主要对用户登录的 认证服务的认证接口的测试用例进行要求
 *  验收，程序通过功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.gateway.authentication.api.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @Class AuthenticationControllerTest
 * @Description
 * @Author Administrator
 * @Date 2022-10-13  13:47
 * @version 1.0.0
 */
class AuthenticationControllerTest {
    private final static String rightCode = "100002";
    private final static String rightPsw = "sd365sd365";
    private final static String rightAccount = "BOSSSOFT";
    @Autowired
    private AuthenticationController authenticationController;
    /**
     * @Description:
     * 1、网关配置转发请求/user/loing 到 /user/token ,该方法主要是对认证服务的
     * 认证结果生成进行测试
     * 2、该方法进行单元测试时候必须保证 用户中心的auth（）接口能被调用
     * 3、因为用户中心的auth()方法已经进行过单元测试，所以本次测试聚焦在token是否正确上
     * @Author: Administrator
     * @DATE: 2022-10-13  13:47
     */
    @Test
    void getToken() {
        // 测试步骤1
            //传入正确的账号　密码　　和　租户,调用中心中饭auth返回结果符合预期，重点断言token符合预期，
        authenticationController.doAuth(rightCode,rightPsw,rightAccount);

        // 测试步骤2
           // 测试正确的账号，错误的密码 和 租户，调用中心中饭auth返回结果符合预期，断言
            //没有生成token 且返回的UserVO 的 code 符合预期 401 的状态码

    }
}