package com.sd365.gateway.authentication.api.service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sd365.App;
import com.sd365.AuthenticationApplication;
import com.sd365.gateway.authen.pojo.vo.UserVO;
import com.sd365.gateway.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthenticationApplication.class})
public class AuthenticationServiceImplTest {

    /**
     * 测试对象
     */
    @Resource
    private AuthenticationService authenticationService;
    @Test
    public void doAuth() {
       HashMap authResult= authenticationService.doAuth("100002","BOSSSOFT","sd365sd365");
       String jsonString=new JSONObject((HashMap)authResult.get("body")).toJSONString();
       UserVO userVO= JSON.parseObject(jsonString,UserVO.class);

       Assert.assertTrue(userVO.getCode()==200);
       Assert.assertTrue(userVO.getMsg().equals("登录成功"));
       Assert.assertTrue(userVO.getRoleIds().get(0)==1337645864428109824L);
       log.info(userVO.toString());
    }
}