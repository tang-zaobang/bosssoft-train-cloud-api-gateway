package com.sd365;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertTrue;
/**
 * Unit test for simple App.
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class AppTest
{
    /**
     * Rigorous Test :-)
     */
    @Resource(name = "tokenRedisTemplate")
    RedisTemplate redisTemplate;
    @Test
    public void shouldAnswerWithTrue()
    {
        redisTemplate.opsForValue().set("keyday01", "keyday01", 60);
        assertTrue( true );
    }
    @Test
    public void shouldAnswerWithTrue2()
    {
        String key = "remote:cache:token:user:1337970508645400576";

        ValueOperations<String, String> valueOps = redisTemplate.opsForValue();
        String value = valueOps.get(key);
        log.info(value);
    }
}
