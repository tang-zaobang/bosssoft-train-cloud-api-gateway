package com.sd365.gateway.authentication.api;

import com.sd365.gateway.authen.pojo.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;


/**
 *
 * @author TangZaoBang
 * @date 2023/06/15 13:41
 * @version 1.0
 */
@Api(tags = "认证管理", value = "/auth")
@RequestMapping("/auth")
@CrossOrigin
public interface AuthenticationApi {
    /**
     *  登录验证
     * @param code  登录输入的工号
     * @param account 登录输入的 租户账号
     * @param password 登录输入的 密码
     * @return 这个返回值将被统一应答包装为 CommonResponse<UserVO> 的 JSON String 类型
     */
    @ApiOperation(tags = "获取token", value = "/token")
    @GetMapping("token")
    @CrossOrigin
    UserVO doAuth(@RequestParam("code") String code,
                  @RequestParam("account") String account,
                  @RequestParam("password") String password);
}
