package com.sd365.gateway.core.controller;

import lombok.extern.slf4j.Slf4j;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;



@Slf4j
@RestController
@RefreshScope
public class DefaultController {

    @RequestMapping("/defaultfallback")
    public Map<String, String> defaultfallback() {
        log.info("服务已被熔断");
        Map<String, String> map = new HashMap<>(100);
        map.put("Code", "fail");
        map.put("Message", "服务异常");
        map.put("result", "");
        return map;
    }
}
