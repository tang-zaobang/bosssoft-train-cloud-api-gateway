package com.sd365.gateway.core.filter;

import com.alibaba.fastjson.JSON;
import com.sd365.gateway.core.bo.UserBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;

/**
 * @author Administrator
 * @version 1.0.0
 * @class WhiteListFilter
 * @classdesc 用于特殊场景下的全网降级（比如正式环境的验证），降级参数在配置中心配置
 * @date 2021-11-18  11:58
 * @see
 * @since
 */
@Slf4j
@RefreshScope
@Component
public class WhiteListFilter implements GlobalFilter, Ordered {
    /**
     * 设置系统运行模式 Normal 为正常 ，Degrade 为降级
     */

    @Value("${system.run.level}")
    private String systemRunLevel;
    /**
     * 系统降级常亮将在配置中心配置可以触发系统降级
     */
    private final static String RUN_LEVEL_NORMAL = "Normal";
    private final static String RUN_LEVEL_DEGRADE = "Degrade";
    private final static String MSG = "系统已降级,您不可访问";
    /**
     * 用于访问白名单
     */
    @Autowired
    private RedisTemplate redisTemplate;
    @Value("${keypres}")
    private String KEY_PRES;

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * 如果是登录请求则立刻进行白名单判断 属于白名单则允许请求继续访问
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("{}",this);
        String URI = exchange.getRequest().getURI().toString();
        Boolean flag = false;
        if (URI.contains("user/info") || URI.contains("user/logout")) {
            return chain.filter(exchange);
        }
        // 如果系统降级 只有白名单允许通过
        if (RUN_LEVEL_DEGRADE.equalsIgnoreCase(systemRunLevel)) {
            if (exchange.getRequest().getURI().toString().contains("/user/login")) {
                MultiValueMap<String, String> userInfo = exchange.getRequest().getQueryParams();
                UserBo userBO = new UserBo(userInfo.getFirst("code"), userInfo.getFirst("password"), null, userInfo.getFirst("tenantId"));
                if (checkWhiteList(userBO)) {// 属于白名单不允许放行
                    return chain.filter(exchange);
                } else { //不是白名单全部不让通过 就是停止请求
                    byte[] bytes={};
                    try {
                        bytes = MSG.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {

                    }
                    return exchange.getResponse().writeWith(Flux.just(exchange.getResponse().bufferFactory().wrap(bytes)));
                }
            } else {
                // 非登录界面 则解析token进一步做白名单判断
                //解析出token 如果有 判断token的用户是否白名单 是 则放行否则登录
                return chain.filter(exchange);
            }
        } else //非降级模式不进行拦截
        {
            return chain.filter(exchange);
        }
    }


    /**
     * 数据来源描述：用户中心增加黑白名单模块，界面增加黑白名后将黑白名单list对象存入redis<br>
     * 本方法主要操作描述： 从redis中取得判断是否存在黑名单存在则终止请求返回应答码否则放行
     *
     * @param userBO
     * @return
     */
    private Boolean checkWhiteList(UserBo userBO) {
        Object o = redisTemplate.opsForValue().get(KEY_PRES + userBO.getCode());
        if (ObjectUtils.isEmpty(o)) {
            return false;
        } else {
            UserBo whitelist = JSON.parseObject(JSON.toJSONString(o), UserBo.class);
            if (!ObjectUtils.isEmpty(whitelist)) {
                if (whitelist.getType().equals("白名单")) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    }
}
