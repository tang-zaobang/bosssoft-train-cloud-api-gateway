package com.sd365.gateway.core.limit;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Administrator
 * @version 1.0.0
 * @class HostAndKeyResolver
 * @classdesc 依据访问的URL来限流
 * @date 2021-11-18  20:12
 * @see
 * @since
 */

public class UriKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        return Mono.just(exchange.getRequest().getURI().getPath());
    }


}
