/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: IGatewayEntryHandler
 * Author: Administrator
 * Date: 2023-04-17 15:13:26
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-17 15:13:26
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.core.handler;

/**
 * @InterfaceName: IGatewayEntryHandler
 * @Description: 负责网关的请求的守护和分发
 * 主要包含 1、黑名单检测 2、token合法性判断 3、请求转发
 * @Author: Administrator
 * @Date: 2023-04-17 15:13
 **/
public interface IGatewayEntryHandler {
    /**
     *  检查配置文件中的白名单资源（存在nacos配置文件中）
     * @param URI 请求地址
     * @return true匹配找到 false匹配没找到
     */
     boolean detectConfigWhiteList(String URI);

    /**
     *  检查请求的地址是否开放设置的 资源表中（存数据库），如果在允许开放请求
     * @param URI 请求地址
     * @return true匹配找到 false 匹配没找到
     */
     boolean detectCommonResource(String URI);

    /**
     * 实现验证签名
     *  验证签名
     * @param token 前端发送的token
     * @return true签名验证合法 false 签名不合法
     */
     boolean verifySign(String token);

    /**
     *  引导请求去鉴权
     * @param token  从前端发送的token
     * @param uri 从前端请求过来的uri
     * @return 具体参考 BusinessResultConsts
     */
     Integer dispatch2Authorize(String token, String uri);

}
