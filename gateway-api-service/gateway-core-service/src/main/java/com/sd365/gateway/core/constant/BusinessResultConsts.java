/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: BusinessResultConsts
 * Author: Administrator
 * Date: 2023-04-26 16:29:01
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-26 16:29:01
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.gateway.core.constant;

/**
 * @ClassName: BusinessResultConsts
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-04-26 16:29
 **/
public class BusinessResultConsts {
    /**
     * 过期 ，过期时间由一个 当前日期+指定的天数构成（可以配置），过期提醒时间为
     * 当前日期+指定的天数构成（可以配置）-指定的提醒天数
     */
    public static final int TOKEN_EXPIRE_TURE=1;
    /**
     * 即将过期 客户端可以发起续约
     */
    public static final int TOKEN_EXPIRE_NEARLY=0;
    /**
     *  没有过期不需要任务续约动作
     */
    public static final int TOKEN_EXPIRE_FALSE=-1;

    /**
     *  认证成功
     */
    public static final int AUTHOR_RESULT_TRUE=1;

    /**
     *  认证成功
     */
    public static final int AUTHOR_RESULT_MATCH_RESOURCE_FALSE=-1;

    /**
     *  认证 token 过期
     */
    public static final int AUTHOR_RESULT_TOKEN_EXPIRE=-2;


    /**
     *  认证 token 需要续约
     */
    public static final int AUTHOR_RESULT_TOKEN_NEARLY=-3;




}
